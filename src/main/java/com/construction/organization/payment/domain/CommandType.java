package com.construction.organization.payment.domain;

public enum CommandType {
    CREATE,
    UPDATE,
    SUBMIT,
    VERIFY,
    CONFIRM,
    REVIEW,
    APPROVE,
    CASH_OUT,
    REJECT
}
