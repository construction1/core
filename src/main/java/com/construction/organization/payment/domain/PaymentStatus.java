package com.construction.organization.payment.domain;

public enum PaymentStatus {
    OPEN,
    SUBMITTED,
    VERIFIED,
    CONFIRMED,
    REVIEWED,
    APPROVED,
    REJECTED,
    PAID
}
