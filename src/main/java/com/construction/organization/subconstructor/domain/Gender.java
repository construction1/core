package com.construction.organization.subconstructor.domain;

public enum Gender {
    MALE,
    FEMALE
}
