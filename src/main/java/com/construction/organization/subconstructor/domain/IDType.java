package com.construction.organization.subconstructor.domain;

public enum IDType {
    NATIONAL_ID,
    PASSPORT,
    FAMILY_BOOK
}
