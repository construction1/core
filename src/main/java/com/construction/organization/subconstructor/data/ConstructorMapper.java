package com.construction.organization.subconstructor.data;

import com.construction.organization.subconstructor.domain.SubConstructor;
import com.construction.persistence.mapper.DtoMapper;
import org.springframework.stereotype.Component;

@Component
public class ConstructorMapper extends DtoMapper<SubConstructor, ConstructorDto> {
}
