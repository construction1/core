package com.construction.user.authorization.domain;

public enum ActionName {
    ALL,
    FULL,
    READ,
    CREATE,
    UPDATE,
    DELETE,
    ASSIGN,
    ASSIGN_ASSIGNED,
    VERIFY,
    REVIEW,
    APPROVE,
    SUBMIT,
    CONFIRM,
    CASH_OUT
}