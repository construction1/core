package com.construction.user.authentication.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    String userName;
    @Email
    String email;
    String mobile;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    String password;
    Long roleId;
}
