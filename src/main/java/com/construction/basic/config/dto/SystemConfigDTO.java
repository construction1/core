package com.construction.basic.config.dto;

import lombok.Data;

@Data
public class SystemConfigDTO {
    private String code;
    private String value;
}