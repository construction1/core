package com.construction.feature.task.dto.mapper;

import com.construction.feature.task.domain.BOQ;
import com.construction.feature.task.dto.BOQData;
import com.construction.persistence.mapper.DtoMapper;
import org.springframework.stereotype.Component;

@Component
public class BOQDataMapper extends DtoMapper<BOQ, BOQData> {
}
