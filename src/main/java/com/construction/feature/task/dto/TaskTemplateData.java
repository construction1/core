package com.construction.feature.task.dto;

import com.construction.persistence.dto.EntityData;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "task_template")
@Immutable
public class TaskTemplateData extends EntityData {

    private String name;

    private String code;

    @OneToMany
    @JoinColumn(name = "parent_id")
    private List<TaskTemplateData> child;

    private boolean firstLevel;

    private boolean leaf;
}
