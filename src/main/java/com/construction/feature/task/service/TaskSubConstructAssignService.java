package com.construction.feature.task.service;

import com.construction.feature.task.domain.Task;
import com.construction.feature.task.domain.TaskSubConstructorAssign;
import com.construction.feature.task.repository.TaskRepository;
import com.construction.feature.task.repository.TaskSubConstructorAssignRepository;
import com.construction.organization.subconstructor.domain.SubConstructor;
import com.construction.organization.subconstructor.repository.SubConstructorRepository;
import com.construction.persistence.domain.ObjectStatus;
import com.construction.persistence.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class TaskSubConstructAssignService {

    private final TaskSubConstructorAssignRepository repository;
    private final TaskRepository taskRepository;
    private final SubConstructorRepository subConstructorRepository;

    public List<TaskSubConstructorAssign> assign(final Long taskId, final List<Long> subIds) {
        final var task = taskRepository.findById(taskId).orElseThrow(() -> new ResourceNotFoundException(Task.class, taskId));
        final var subs = subConstructorRepository.findAllById(subIds);

        repository.deleteWithTaskId(taskId);

        final var assign = subs.stream()
                .filter(sub -> ObjectStatus.APPROVED.equals(sub.getStatus()))
                .map(sub -> build(task, sub))
                .collect(Collectors.toList());

        return repository.saveAll(assign);
    }

    public void unAssign(Long taskId, final List<Long> subIds) {
        subIds.forEach(id -> {
            repository.deleteByTaskIdAndSubConstructorId(taskId, id);
        });
    }

    private TaskSubConstructorAssign build(final Task task, final SubConstructor subConstructor) {
        return new TaskSubConstructorAssign()
                .setCreatedAt(LocalDateTime.now())
                .setTask(task)
                .setSubConstructor(subConstructor);
    }
}
