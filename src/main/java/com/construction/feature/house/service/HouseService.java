package com.construction.feature.house.service;

import com.construction.appconfiguration.utils.ApplicationSecurityContext;
import com.construction.exception.UnAuthorizeException;
import com.construction.feature.FilterType;
import com.construction.feature.house.domain.House;
import com.construction.feature.house.domain.HouseAssign;
import com.construction.feature.house.repository.HouseAssignRepository;
import com.construction.feature.house.repository.HouseRepository;
import com.construction.persistence.domain.AssignFor;
import com.construction.persistence.domain.ObjectStatus;
import com.construction.persistence.exception.ResourceNotFoundException;
import com.construction.persistence.service.EntityDataMapper;
import com.construction.persistence.utils.EntityValidator;
import com.construction.persistence.utils.SFWhere;
import com.construction.user.authentication.service.AppUserService;
import com.construction.user.authorization.domain.ActionName;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class HouseService {

    private final HouseRepository repository;
    private final EntityDataMapper dataMapper;
    private final ApplicationSecurityContext context;
    private final EntityValidator<House> validator;
    private final HouseAssignRepository assignRepository;
    private final AppUserService userService;

    public House save(House house) {
        return repository.save(house);
    }

    public void deleteById(Long id) {
        var house = getById(id);
        validator.validateStatus(house, ActionName.DELETE);
        repository.delete(house);
    }

    public House getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(House.class, id));
    }

    public List<House> getAll() {
        var houses = repository.findAll();
        return houses.stream().distinct().collect(Collectors.toList());
    }

    public Page<House> search(House house, Pageable pageable) {
        return repository.findAll(SFWhere.and(house).build(), pageable);
    }

    public List<House> getAssigned() {
        return assignRepository.findAllByAppUser(context.authenticatedUser())
                .stream()
                .map(HouseAssign::getHouse)
                .collect(Collectors.toList());
    }

    public List<House> getPendingForVerify() {
        return repository.findPendingForVerify(context.authenticatedUser().getId());
    }

    public List<House> getPendingForApprove() {
        return repository.findPendingForApprove(context.authenticatedUser().getId());
    }

    public Page<House> getAllPending(Pageable pageable) {
        return repository.findAllPending(context.authenticatedUser().getId(), pageable);
    }

    public Page<House> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Page<House> getAll(Pageable pageable, FilterType type) {
        final var user = context.authenticatedUser();
        if (user == null) {
            throw new UnAuthorizeException();
        }
        switch (type) {
            case PENDING_FOR_APPROVE:
                return repository.findPendingForApprove(user.getId(), pageable);
            case PENDING_FOR_VERIFY:
                return repository.findPendingForVerify(user.getId(), pageable);
            case ASSIGNED:
                return repository.findAssignedHouse(user.getId(), pageable);
            case OWNED:
                return repository.findAllByCreatedBy(user, pageable);
            default:
                return repository.findAll(pageable);
        }
    }

    public House updateById(Long id, House source) {
        var target = getById(id);
        validator.validateStatus(target, ActionName.UPDATE);
        target = dataMapper.mapObject(source, target, House.class);
        return repository.save(target);
    }

    public HouseAssign assign(Long id, Long userId, AssignFor assignFor) {
        var house = getById(id);
        var user = userService.getById(userId);
        var houseAssign = new HouseAssign().setHouse(house);
        houseAssign.setAppUser(user);
        houseAssign.setAssignFor(assignFor);
        return assignRepository.save(houseAssign);
    }

    public void unAssign(Long id, Long userId) {
        var houseAssign = assignRepository.findByHouseIdAndAppUserId(id, userId).orElseThrow();
        assignRepository.delete(houseAssign);
    }

    public House verify(Long id) {
        var house = getById(id);
        validator.validateStatus(house, ActionName.VERIFY);
        house.setStatus(ObjectStatus.VERIFIED);
        house.setVerifiedAt(LocalDateTime.now());
        house.setVerifiedBy(context.authenticatedUser());
        return repository.save(house);
    }

    public List<House> verifyAll(List<Long> ids) {
        return ids.stream().map(this::verify).collect(Collectors.toList());
    }

    public House approve(Long id) {
        var house = getById(id);
        validator.validateStatus(house, ActionName.APPROVE);
        house.setStatus(ObjectStatus.APPROVED);
        house.setApprovedAt(LocalDateTime.now());
        house.setApprovedBy(context.authenticatedUser());
        return repository.save(house);
    }

    public List<House> approveAll(List<Long> ids) {
        return ids.stream().map(this::approve).collect(Collectors.toList());
    }
}