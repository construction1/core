package com.construction.feature;

public enum FilterType {
    ALL,
    PENDING_FOR_VERIFY,
    PENDING_FOR_APPROVE,
    ASSIGNED,
    OWNED
}
