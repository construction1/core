package com.construction.persistence.domain;

public enum SimpleObjectStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
