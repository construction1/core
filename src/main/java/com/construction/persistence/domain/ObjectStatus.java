package com.construction.persistence.domain;

public enum ObjectStatus {
    OPEN,
    VERIFIED,
    APPROVED,
    CLOSED,
    EXPIRED,
    DELETED
}
