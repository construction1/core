package com.construction.persistence.domain;

public enum AssignFor {
    VERIFY,
    APPROVE,
    READ,
    READ_WRITE // or delete
}
