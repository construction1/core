package com.construction.persistence.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class IdListBatch {
    @NotNull
    private List<Long> ids;
    @NotNull
    private List<Long> sids;
}
