package com.construction.persistence.constants;

import java.math.BigDecimal;

public class NumberConstants {
    public static final BigDecimal HUNDRED = BigDecimal.valueOf(100L);
    public static final Float HUNDRED_FLOAT = 100F;
}
