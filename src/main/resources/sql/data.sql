insert into permission(id,version,action_name,entity_name,scope,code_name) values (1,0,'ALL','ALL','ALL','ALL_ALL_ALL');
insert into permission(id,version,action_name,entity_name,scope,code_name) values (2,0,'READ','ALL','ALL','READ_ALL_ALL');

INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(1,0,'ALL','ALL_ALL_ALL',NULL,'ALL','ALL')
,(2,0,'READ','READ_ALL_ALL',NULL,'ALL','ALL')
,(3,0,'READ','READ_ALL_USER',NULL,'USER','ALL')
,(4,0,'CREATE','CREATE_ALL_USER',NULL,'USER','ALL')
,(5,0,'UPDATE','UPDATE_ALL_USER',NULL,'USER','ALL')
,(6,0,'DELETE','DELETE_ALL_USER',NULL,'USER','ALL')
,(12,0,'READ','READ_ALL_PROJECT',NULL,'PROJECT','ALL')
,(14,0,'CREATE','CREATE_ALL_PROJECT',NULL,'PROJECT','ALL')
,(16,0,'UPDATE','UPDATE_ALL_PROJECT',NULL,'PROJECT','ALL')
,(17,0,'UPDATE','UPDATE_ASSIGNED_PROJECT',NULL,'PROJECT','ASSIGNED')
;
INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(18,0,'DELETE','DELETE_ALL_PROJECT',NULL,'PROJECT','ALL')
,(19,0,'DELETE','DELETE_ASSIGNED_PROJECT',NULL,'PROJECT','ASSIGNED')
,(20,0,'ASSIGN','ASSIGN_ALL_PROJECT',NULL,'PROJECT','ALL')
,(21,0,'ASSIGN','ASSIGN_ASSIGNED_PROJECT',NULL,'PROJECT','ASSIGNED')
,(22,0,'VERIFY','VERIFY_ALL_PROJECT',NULL,'PROJECT','ALL')
,(23,0,'VERIFY','VERIFY_ASSIGNED_PROJECT',NULL,'PROJECT','ASSIGNED')
,(24,0,'APPROVE','APPROVE_ALL_PROJECT',NULL,'PROJECT','ALL')
,(25,0,'APPROVE','APPROVE_ASSIGNED_PROJECT',NULL,'PROJECT','ASSIGNED')
,(28,0,'READ','READ_ALL_TASK',NULL,'TASK','ALL')
,(30,0,'CREATE','CREATE_ALL_TASK',NULL,'TASK','ALL')
;
INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(32,0,'UPDATE','UPDATE_ALL_TASK',NULL,'TASK','ALL')
,(33,0,'UPDATE','UPDATE_ASSIGNED_TASK',NULL,'TASK','ASSIGNED')
,(34,0,'DELETE','DELETE_ALL_TASK',NULL,'TASK','ALL')
,(35,0,'DELETE','DELETE_ASSIGNED_TASK',NULL,'TASK','ASSIGNED')
,(36,0,'ASSIGN','ASSIGN_ALL_TASK',NULL,'TASK','ALL')
,(37,0,'ASSIGN','ASSIGN_ASSIGNED_TASK',NULL,'TASK','ASSIGNED')
,(38,0,'VERIFY','VERIFY_ALL_TASK',NULL,'TASK','ALL')
,(39,0,'VERIFY','VERIFY_ASSIGNED_TASK',NULL,'TASK','ASSIGNED')
,(40,0,'APPROVE','APPROVE_ALL_TASK',NULL,'TASK','ALL')
,(41,0,'APPROVE','APPROVE_ASSIGNED_TASK',NULL,'TASK','ASSIGNED')
;
INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(44,0,'READ','READ_ALL_HOUSE',NULL,'HOUSE','ALL')
,(45,0,'ASSIGN','ASSIGN_ALL_HOUSE',NULL,'HOUSE','ALL')
,(46,0,'CREATE','CREATE_ALL_HOUSE',NULL,'HOUSE','ALL')
,(47,0,'ASSIGN','ASSIGN_ASSIGNED_HOUSE',NULL,'HOUSE','ASSIGNED')
,(48,0,'UPDATE','UPDATE_ALL_HOUSE',NULL,'HOUSE','ALL')
,(49,0,'UPDATE','UPDATE_ASSIGNED_HOUSE',NULL,'HOUSE','ASSIGNED')
,(50,0,'DELETE','DELETE_ALL_HOUSE',NULL,'HOUSE','ALL')
,(51,0,'DELETE','DELETE_ASSIGNED_HOUSE',NULL,'HOUSE','ASSIGNED')
,(54,0,'VERIFY','VERIFY_ALL_HOUSE',NULL,'HOUSE','ALL')
,(55,0,'VERIFY','VERIFY_ASSIGNED_HOUSE',NULL,'HOUSE','ASSIGNED')
;
INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(56,0,'APPROVE','APPROVE_ALL_HOUSE',NULL,'HOUSE','ALL')
,(57,0,'APPROVE','APPROVE_ASSIGNED_HOUSE',NULL,'HOUSE','ASSIGNED')
,(60,0,'READ','READ_ALL_STREET',NULL,'STREET','ALL')
,(61,0,'ASSIGN','ASSIGN_ASSIGNED_STREET',NULL,'STREET','ASSIGNED')
,(62,0,'CREATE','CREATE_ALL_STREET',NULL,'STREET','ALL')
,(63,0,'ASSIGN','ASSIGN_ALL_STREET',NULL,'STREET','ALL')
,(64,0,'UPDATE','UPDATE_ALL_STREET',NULL,'STREET','ALL')
,(65,0,'UPDATE','UPDATE_ASSIGNED_STREET',NULL,'STREET','ASSIGNED')
,(66,0,'DELETE','DELETE_ALL_STREET',NULL,'STREET','ALL')
,(67,0,'DELETE','DELETE_ASSIGNED_STREET',NULL,'STREET','ASSIGNED')
;
INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(70,0,'VERIFY','VERIFY_ALL_STREET',NULL,'STREET','ALL')
,(71,0,'VERIFY','VERIFY_ASSIGNED_STREET',NULL,'STREET','ASSIGNED')
,(72,0,'APPROVE','APPROVE_ALL_STREET',NULL,'STREET','ALL')
,(73,0,'APPROVE','APPROVE_ASSIGNED_STREET',NULL,'STREET','ASSIGNED')
,(76,0,'READ','READ_ALL_BOQ',NULL,'BOQ','ALL')
,(78,0,'CREATE','CREATE_ALL_BOQ',NULL,'BOQ','ALL')
,(80,0,'UPDATE','UPDATE_ALL_BOQ',NULL,'BOQ','ALL')
,(82,0,'DELETE','DELETE_ALL_BOQ',NULL,'BOQ','ALL')
,(83,0,'SUBMIT','SUBMIT_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(84,0,'VERIFY','VERIFY_ALL_PAYMENT',NULL,'PAYMENT','ALL')
;
INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(85,0,'CONFIRM','CONFIRM_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(86,0,'REVIEW','REVIEW_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(87,0,'APPROVE','APPROVE_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(88,0,'CASH_OUT','CASH_OUT_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(92,0,'READ','READ_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(94,0,'CREATE','CREATE_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(96,0,'UPDATE','UPDATE_ALL_PAYMENT',NULL,'PAYMENT','ALL')
,(98,0,'DELETE','DELETE_ALL_PAYMENT',NULL,'PAYMENT','ALL');

INSERT INTO construction.permission (id,version,action_name,code_name,description,entity_name,`scope`) VALUES
(105,0,'CREATE','CREATE_ALL_SUB_CONSTRUCTOR',NULL,'SUB_CONSTRUCTOR','ALL')
,(106,0,'UPDATE','UPDATE_ALL_SUB_CONSTRUCTOR',NULL,'SUB_CONSTRUCTOR','ALL')
,(107,0,'DELETE','DELETE_ALL_SUB_CONSTRUCTOR',NULL,'SUB_CONSTRUCTOR','ALL')
,(108,0,'VERIFY','VERIFY_ALL_SUB_CONSTRUCTOR',NULL,'SUB_CONSTRUCTOR','ALL')
,(109,0,'APPROVE','APPROVE_ALL_SUB_CONSTRUCTOR',NULL,'SUB_CONSTRUCTOR','ALL')
;

insert into user_role(id,`version`,name) values (1,0,'admin');

insert into role_permission values (1,1);

insert into users(id,`version`,user_name,email,`password`,status,role_id) values (1,0,'admin','admin@mail.com','$2a$10$BCcz5D3246cIiRbaaNzDiO5pz4.hMYNl5/YHFo8sYkECLZUGmBo22','ACTIVE',1);