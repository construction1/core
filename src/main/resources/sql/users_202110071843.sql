INSERT INTO construction.users (id,version,email,mobile,password,status,user_name,role_id) VALUES
	 (1,7,'admin@mail.com','012345678','$2a$10$PTD6KPCVDSyP152z8kf97.7azxWZEY7n9cxPsXbkGgA.ef7fzdpoW','ACTIVE','admin',1),
	 (3379,0,'info@com','012121212','$2a$10$b4Ln/ajrqqrsxh8ZxelyzejPvuBMbJKBVvRjbwETKKEzJnK822f/m','ACTIVE','Tola',3189),
	 (3381,0,'chhe@com','010101010','$2a$10$QsHtSb3AAb0Brfchap5yW.5Vbr0AJCos/Qm.aiOuhN6YmAM6qrSJG','ACTIVE','Chheko',3189),
	 (3382,2,'sokhom@com','012121212','$2a$10$quJlGTxLHtGpVwwDN1UxMuAqQ4YJK4n8odeoSXUlqBRe0kspuzUcC','ACTIVE','Sokhom',3353),
	 (3387,0,'eng@com','015151515','$2a$10$VleFw.LACmED2U3W/WxOCOsxW58.vjvhaxAQyllt5Gcp9azV8lIFm','ACTIVE','Engsmh2',3367),
	 (3388,0,'engsmh3@com','016161616','$2a$10$cejT.qiZm4YbIHpfHSSBZ.EKCMZIhVSHrQm1Z/wontROf63kvXO8.','ACTIVE','Engsmh3',3367),
	 (4225,0,'gm01@smh.com','012345678','$2a$10$MqCwmD9UZ39KMUlYzvnvn.zk8nSZQDM6ewW5oSuDiho/jM2cndOku','ACTIVE','GM',1),
	 (4304,0,'phally@gmail.com','0938832921','$2a$10$.xcaIoFeJ6xaucBd1YjoW.c78nAa7SBQLkNZ6zdv5xgsvT2PWjTe.','ACTIVE','phally',3367),
	 (4380,0,'QS001@smh.com','012345678','$2a$10$IrAV7QoOW.dDHrl0wNl6vu7UdL4KpsmicM8E6Ic2KURcvy4N1fRaa','ACTIVE','QS001',3353),
	 (4394,0,'sm001@smh.com','0123555777','$2a$10$y6ordWji.a/k0XOIW3TWzu1.suDZ8HZ8lhkbYQTlw/vpc1xNmsFdW','ACTIVE','SM001',3189);
INSERT INTO construction.users (id,version,email,mobile,password,status,user_name,role_id) VALUES
	 (4591,0,'kemun.dev@gmail.com','0963825719','$2a$10$fhpJnUDmIFenXyOs7BM6g.c7rBsCnVN.lq5YrM.86XHXhQS.Eirc.','ACTIVE','kemun',1),
	 (4597,0,'chanheng.sng3@gmail.com','012813335','$2a$10$/vLNusgT1zukF8cSzD/Y0uZE/0LevWewt4npFw6pm.a5SimOOhtZm','ACTIVE','chanheng',4592),
	 (4598,0,'Eng01@smh.com.kh','012345678','$2a$10$TYRuG.ZdQQ3KT6X1GitJWOrLDHwEI.NzMPFBP9QQ2jo7y5eekTbwi','ACTIVE','Eng01',3367),
	 (4823,0,'somathea@gmail.com','093323433','$2a$10$3jG2zvEX4gNAyqCZHykxBOfReHJPwm2f7/0IK6N12mNRNPDAjyXLa','ACTIVE','Somathea',4340);